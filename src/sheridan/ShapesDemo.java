package sheridan;

import static org.junit.Assert.assertEquals;

public class ShapesDemo {

	public static void calculateArea( Rectangle r ) {
		r.setWidth( 2 );
		r.setHeight( 3 );
		
		assertEquals( "Area calculation is incorrect", r.getArea( ), 6 );
	}
	
	public static void main(String[] args) {
		//rectangle passed
		ShapesDemo.calculateArea( new Rectangle( ) );
		
		// square passed
		ShapesDemo.calculateArea( new Square( ) );
	}
}
