package sheridan;

public class Square extends Rectangle {

	public int getArea() {
		return getWidth() * getWidth(); //height and width are the same for square
	}
	
	public int getPerimeter() {
		return 4 * getWidth(); //height and width are the same for square
	}
	
}
